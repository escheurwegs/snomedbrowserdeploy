#!/bin/bash

#Get latest Node (stable): https://github.com/nodejs/help/wiki/Installation

#or..
sudo apt-get install -U npm
sudo npm install -g npm@latest-6
npm install grunt --save-dev

cd sct-browser-frontend-3.22.0
npm install --force && ./node_modules/grunt-cli/bin/grunt
#npm install
#grunt