#!/bin/bash

rsync -av . /mnt/data/deploy
sudo mkdir -p /mnt/data/docker_shares/elasticsearch
sudo chmod 777 /mnt/data/docker_shares/elasticsearch
sudo chmod -R 777 /mnt/data/deploy/sct-browser-frontend-3.22.0

echo "vm.max_map_count=262144" | sudo tee /etc/sysctl.d/99_maxmapcount.conf
sudo sysctl -system

#check current cat /proc/sys/vm/max_map_count