Snomed CT browser op een server plaatsen
----------------------------------------

Om onze eigen snomed-extensie zichtbaar te maken, hebben we een eigen
counterpart van de snomed-ct browser nodig. Deze is de counterpart van
(https://browser.ihtsdotools.org/) en te vinden op
(http://snomed.uza.be)

We gaan de Snomed CT browser adhv docker online brengen, met drie
containers in docker-compose:

-   snomed\_browser: de frontend
-   Elasticsearch: de databank waarop de concepten worden gezocht
-   Snowstorm: de API die communiceert met de DB

De packages zijn bereikbaar via:

-   <https://github.com/IHTSDO/sct-browser-frontend>
-   <https://github.com/IHTSDO/snowstorm>

Deze setup kan ook gebruikt worden voor een 'authoring server', maar
deze is nog niet voldoende gedocumenteerd om makkelijk op te zetten. Dit
wordt later verder uitgepluisd.

De releases worden geupload adhv een 'full' release in het begin, en als
een delta bij updates. Om deze te uploaden zijn scripts beschikbaar. Er
moet altijd één release worden geupload, waarop meerdere extensies
kunnen geent worden.

De extensies moeten het officiele formaat van snomed CT volgen om
opgeladen te kunnen worden.

### Server voorbereiden

Een server voorzien met genoeg werkgeheugen (20GB/8vCPUs). Op deze
server moet volgend commando worden uitgevoerd, om elasticsearch te
kunnen launchen:

``` {.bash}
echo "vm.max_map_count=262144" | sudo tee /etc/sysctl.d/99_maxmapcount.conf
sudo sysctl -system
```

-   

We plaatsen het mapje `deploy` op de server. Vanuit deze map zal alles
worden gemount binnen de server waar we aan willen kunnen. De gemounte
mappen moeten ook zeker voldoende rechten hebben om docker toe te laten
hieraan te kunnen:

    sudo chmod -R 777 data_elastic
    sudo chmod -R 777 nginx_conf_d
    sudo chmod -R 777 sct-browser-frontend-3.22.0

Als je nieuwe edities van de browser en snowstorm installeert, moet je
eerst `1_build_browser.sh` runnen. Hier ga je met node.js de browser
voorbereiden (dit moet niet op de server zelf, je kan ook gewoon het
resultaat kopieren. Je hebt deze software niet nodig om het zelf te
runnen.) Dit voert volgende acties uit in de browsermap; om alles klaar
te maken voor deployment:

    cd sct-browser-frontend-3.22.0
    npm install --force && ./node_modules/grunt-cli/bin/grunt

Hierna bereiden we `docker-compose.yml` voor binnen de `deploy` map.
Hier moeten we verwijzen naar de juiste sct-browser-frontend map en
kunnen we ook de naammgeving van containers wijzigen indien we parallel
met de vorige versie willen draaien op de server.

### Nieuwe snomed-ct-browser-frontend

We gebruiken aangepaste versies van de Dockerfile en het entrypoint,
waarbij we zorgen dat onze eigen nginx.conf wordt gebruikt en de grunt
build al op voorhand is uitgevoerd. De voornaamse bestanden bij deze
wijziging zijn:

``` {.bash}
docker/docker-entrypoint.sh
Dockerfile
```

Snowstorm kan je prebuild deployen, hiervoor plaatsen we in de map
`deploy/snowstorm/target` een jar van de gecompileerde snowstorm. Deze
kunnen we dan rechtstreeks includeren in de Dockerfile in plaats van
eerst een build container aan te maken.

### Deployment zelf

        docker-compose up -d
        #of, om te debuggen wat er evt misgaat bij individuele containers.
        docker-compose up elasticsearch
        docker-compose up snowstorm
        docker-compose up snomed_browser

Na het uploaden van de release files moet snowstorm op read-only komen
te staan, mits de API publiek beschikbaar is.

        nano /mnt/data/deploy/snowstorm.properties
        docker-compose restart snowstorm

Bereikbaarheid browser
----------------------

De snomed browser zou bereikbaar moeten zijn op poort 80, de API op
poort 8080. Als je met SSL wil werken kan je een reverse proxy nginx
opzetten op het host systeem; of de inkomende nginx wijzigen zodat de
certificaten makkelijk kunnen vervangen worden.

Een lege browser vullen met concepten
-------------------------------------

Hierna gaan we de API van snowstorm gebruiken om de browser op te
vullen. Dit doen we door de juiste endpoints aan te spreken. Meer
details vind je
(https://github.com/IHTSDO/snowstorm/blob/master/docs/loading-snomed.md
| hier) en
(https://github.com/IHTSDO/snowstorm/blob/master/docs/updating-snomed-and-extensions.md
| hier)

De releases kunnen gedownload worden op <https://mlds.ihtsdotools.org/>

We installeren de internationale editie, en de belgische en UZA-release
als extensies. Deze installatie gebeurt via de rest API, waarbij we
eerst een request doen om een extensie te creëren, en daarna deze id
gebruiken om de upload uit te voeren.

### Stap 1: Snowstorm voorbereiden

Pas `snowstorm.properties` in deploy folder aan met nieuwe inhoud:

    snowstorm.rest-api.readonly=false

Herstart snowstorm met `docker-compose restart snowstorm_75`

### Stap 2: Internationale editie aanvullen

We laden snomed-ct releasebestanden in vanop de rest API. Hiervoor
kopieren we de releasefiles naar `/mnt/data/snomed_releases/`

Hierna spreken we endpoint <server>`:`<port>`/imports/` met request:

``` {.json}
{
  "branchPath": "MAIN",
  "createCodeSystemVersion": true,
  "type": "FULL"
}
```

Noteer de import id die toegewezen wordt (format
'd0b30d96-3714-443e-99a5-2f282b1f1b0')

Daarna het bestand uploaden, met de geretourneerde import id:

    curl -X POST --header 'Content-Type: multipart/form-data' --header 'Accept: application/json' -F file=@SnomedCT_InternationalRF2_PRODUCTION_20180131T120000Z.zip 'http://snomed.uza.be:8080/imports/<import id>/archive'

Voortgang kunnen we controleren via:
<server>`:`<port>`/import/`<import id>

### Stap 3: BE Extension

Nadat de internationale versie finished is, dus volledig geimporteerd,
kunnen we de BE extension opladen.

Create codesystem <server>`:`<port>`/codesystems`

    curl -v -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' -d '{
      "branchPath": "MAIN/SNOMEDCT-BE",
      "defaultLanguageCode": "en",
      "countryCode":"be",
      "dependantVersionEffectiveTime": 20210731,
      "name": "Belgium",
      "shortName": "SNOMEDCT-BE"}' 'http://snomed.uza.be:8080/codesystems'

Create import job <server>`:`<port>`/imports/`

    curl -v -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' -d '{
      "branchPath": "MAIN/SNOMEDCT-BE",
      "createCodeSystemVersion": true,
      "type": "FULL"}' 'http://snomed.uza.be:8080/imports'

Upload the datafile

    curl -X POST --header 'Content-Type: multipart/form-data' --header 'Accept: application/json' -F file=@SnomedCT_BelgiumExtensionRF2_PRODUCTION_20200315T120000Z.zip 'http://snomed.uza.be:8080/imports/<import id>/archive'

Voortgang te vinden op `http://snomed.uza.be:8080/imports/`<import id>

### Stap 4: UZA Extensie

Deze maken we hetzelfde als de BE-extensie, maar dan als dependant aan
de BE-extensie:

Create codesystem <server>`:`<port>`/codesystems`

    curl -v -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' -d '{
      "branchPath": "MAIN/SNOMEDCT-BE/SNOMEDCT-UZA",
      "defaultLanguageCode": "en",
      "countryCode":"",
      "dependantVersionEffectiveTime": 20210915,
      "name": "UZA Extension",
      "shortName": "SNOMEDCT-UZA"
    }' 'http://snomed.uza.be:8081/codesystems'

Create import job <server>`:`<port>`/imports/`

    curl -v -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' -d '{
      "branchPath": "MAIN/SNOMEDCT-BE/SNOMEDCT-UZA",
      "createCodeSystemVersion": true,
      "type": "FULL"}' 'http://snomed.uza.be:8080/imports'
'http://10.1.166.102:8080/imports/71f8d590-57cc-44d3-b83b-4dc187436e16/archive'
Import zelf:

    curl -X POST --header 'Content-Type: multipart/form-data' --header 'Accept: application/json' -F file=@SnomedCT_UzaExtensionRF2_ALPHA_20200122T110000Z.zip 'http://snomed.uza.be:8080/imports/8ac5a489-c84d-4a80-b1e9-8728a74d29a2/archive'

### Updating a subset

We updaten op dezelfde manier als we een nieuwe upload doen, behalve dat
we 'snapshot' als metadata gebruiken:

        {
          "branchPath": "MAIN",
          "createCodeSystemVersion": true,
          "type": "Snapshot"
        }

Verder moeten we ook alle 'dependants' op de hoogte brengen van een
hogergelegen update; op endpoint `/codesystems/SNOMEDCT-BE/upgrade`

    {
      "newDependantVersion": 2020-12-12
    }


### Stap 5: Snowstorm terug op readonly

Pas `snowstorm.properties` in deploy folder aan met nieuwe inhoud:

    snowstorm.rest-api.readonly=true

Herstart snowstorm met `docker-compose restart snowstorm_75`



Gebruik van de API
------------------

De API is beschikbaar op snomed.uza.be:8080 en kan gebruikt worden om
applicaties requests op te laten doen om snomed te valideren.
